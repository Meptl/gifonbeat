import click
import imageio
import os
import shutil
import tempfile

@click.command()
@click.argument('gif_path', type=click.Path(exists=True))
@click.argument('gif_bpm', type=int)
@click.argument('audio_path', type=click.Path(exists=True))
@click.argument('audio_bpm', type=int)
@click.argument('output_file')
@click.option('-ss', '--audio-start', help='Start time of audio in seconds', type=float)
@click.option('-t', '--audio-duration', help='Duration of audio in seconds', type=float)
def accelerate(gif_path, gif_bpm, audio_path, audio_bpm, output_file, audio_start=None, audio_duration=None):
    """Merges a gif with an audio file. Uses a bpm value to modify the animation speed.

    Creates an mp4 file.
    """
    ratio = audio_bpm / gif_bpm
    reader = imageio.get_reader(gif_path)
    durations = []
    for frame in reader:
        # Seems the meta duration is in ms whereas the mimwrite duration args
        # are in seconds.
        durations.append(frame.meta.duration / 1000.0 / ratio)

    with tempfile.TemporaryDirectory() as dir:
        tmp_gif = os.path.join(dir, 'foo.gif')
        imageio.mimwrite(tmp_gif, reader, duration=durations)
        tmp_audio = audio_path
        if audio_start or audio_duration:
            audio_start = audio_start or 0.0
            duration_option = f'-t {audio_duration}' if audio_duration else ''
            _, file_extension = os.path.splitext(audio_path)
            tmp_audio = os.path.join(dir, f'bar{file_extension}')
            os.system(f'ffmpeg -y -i {audio_path} -ss {audio_start} {duration_option} -async 1 {tmp_audio}')

        # For whatever reason, the -shortest option of the ffmpeg audio+gif
        # merge command is adding a few bits of silence to the end of the final
        # artifact so we have to specify a specific time duration for the piece.
        final_duration = audio_duration or os.popen('ffprobe -i {tmp_audio} -show_entries format=duration -v quiet -of csv=p=0').read()
        final_artifact = os.path.join(dir, 'baz.mp4')
        os.system(f'ffmpeg -i {tmp_audio} -ignore_loop 0 -i {tmp_gif} -t {final_duration} {final_artifact}')
        shutil.move(final_artifact, output_file)
